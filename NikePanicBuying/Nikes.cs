﻿using Nike.Helper;
using Nike.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using static Nike.EnumHelper.ProductTypesEnum;

namespace NikePanicBuying
{
    public partial class Nikes : Form
    {
        #region 静态数据
        private delegate void SetDtCallback(List<ChoicenessModel> list);
        private bool IsBegin = false;
        private int Count = 0;
        private List<int> Interval = new List<int>() { 1000, 1100, 1200, 1300, 1400, 1500, 1600, 1700, 1800, 1900, 2000, 2100, 2200, 2300, 2400, 2500, 2600, 2700, 2800, 2900, 3000, 3100, 3200, 3300, 3400, 3500, 1500, 1600, 1700, 1800, 1900, 2000, 2100, 2200, 2300, 2400, 2500, 2600, 2700, 2800, 2900, 3000, 1500, 1600, 1700, 1800, 1900, 2000, 2100, 2200 };//抢购的时间间隔
        private List<int> RapidInterval = new List<int>() { 100, 200, 300, 400, 500, 600, 700, 800, 900, 1000, 1100, 1200, 1300, 1400, 1500, 100, 200, 300, 400, 500, 150, 250, 350, 450, 550, 650, 750, 850, 950, 500, 600, 700, 800, 900, 1000, 500, 600, 700, 800, 900, 100, 100, 200, 300, 400, 500, 600, 700, 800, 900 };
        #endregion


        public Nikes()
        {
            InitializeComponent();
        }       
          
        private void Nike_Load(object sender, EventArgs e)
        {
            this.dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;//设置单元格自适应
            Console.SetOut(new TextBoxWriter(this.outputBlock));
            Console.WriteLine("启动程序中...完成");
            initialization();
            monitoring();
        }

        /// <summary>
        /// 显示数据到dataGridView1上
        /// </summary>
        /// <param name="dt"></param>
        private void SetDT(List<ChoicenessModel> dt)
        {
            List<int> NewGoods = new List<int>();
            if (CacheHelper.Exists("dataGridView1"))
            {
                List<string> Cachelist = (List<string>)CacheHelper.GetCache("dataGridView1");
                for (int i = 0; i < 5; i++)
                {
                    if (dt[i].Pid != Cachelist[i])
                    {
                        NewGoods.Add(i);
                    }
                    else
                    {
                        break;
                    }
                }
                for (int s = 5; s < 10; s++)
                {
                    if (dt[s].Pid != Cachelist[s])
                    {
                        NewGoods.Add(s);
                    }
                    else
                    {
                        break;
                    }
                }
            }
            else
            {
                if (this.dataGridView1.InvokeRequired)
                {
                    SetDtCallback d = new SetDtCallback(SetDT);
                    this.Invoke(d, new object[] { dt });
                }
                else
                {
                    this.dataGridView1.DataSource = dt;
                }
                List<string> list = dt.Select(o => o.Pid).ToList();
                CacheHelper.SetCache("dataGridView1", dt.Select(o => o.Pid).ToList(), 0);
            }
            // InvokeRequired需要比较调用线程ID和创建线程ID
            // 如果它们不相同则返回true
            if (NewGoods.Count > 0)
            {
                if (this.dataGridView1.InvokeRequired)
                {
                    SetDtCallback d = new SetDtCallback(SetDT);
                    this.Invoke(d, new object[] { dt });
                }
                else
                {
                    this.dataGridView1.DataSource = dt;                   
                    foreach (var item in NewGoods)
                    {
                        this.dataGridView1.Rows[item].DefaultCellStyle.BackColor = Color.Red;
                    }

                }
                IsBegin = false;
                MessageBox.Show("安排上了，快去抢购!");
            }
        }

        /// <summary>
        /// 刷新数据(子线程刷新数据)
        /// </summary>
        public void initialization()
        {
            CacheHelper.Remove("dataGridView1");
            System.Threading.ThreadPool.QueueUserWorkItem(new WaitCallback(Initialize));
        }

        /// <summary>
        /// 刷新数据(子线程刷新数据执行的方法)
        /// </summary>
        /// <param name="state"></param>
        public void Initialize(object state)
        {
            double milliseconds = 0;
            if (ConnectionHelper.IsHaveInternet(true, out milliseconds))
            {
                Console.WriteLine("连接Nike官网服务器速度:{0}毫秒", milliseconds);
                System.Threading.ThreadPool.QueueUserWorkItem(u =>
                {
                    LoadingData();
                });
            }
            else
            {
                if (ConnectionHelper.IsHaveInternet(false, out milliseconds))
                {
                    Console.WriteLine("无法连接Nike官网");
                }
                else
                {
                    Console.WriteLine("设备网络异常，请检查设备网络！");
                }
            }
        }

        /// <summary>
        /// 循环拉取页面的数据 
        /// </summary>
        public void LoadingData()
        {
            GetGoodsList getGoods = new GetGoodsList();
            List<ChoicenessModel> model = getGoods.getGoodsList(ProductTypes.NikeChoiceness, true);
            List<ChoicenessModel> model1 = getGoods.getGoodsList(ProductTypes.in_stock, true);
            List<ChoicenessModel> model3 = model.Concat(model1).ToList();
            SetDT(model3);
        }

        /// <summary>
        /// 刷新数据
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {            
            initialization();
        }
        

        /// <summary>
        /// 开始监控 停止按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            if (!IsBegin)
            {
                Console.WriteLine("开始监控NIke产品数据");
                Count = 0;
                IsBegin = true;
                button1.Text = "停止";                
            }
            else
            {
                Console.WriteLine("监控数据结束");
                IsBegin = false;
                button1.Text = "开始监控";
            }
        }

        /// <summary>
        /// 开始监视
        /// </summary>
        public void monitoring()
        {
            System.Threading.ThreadPool.QueueUserWorkItem(u =>
            {

                while (true)
                {
                    if (IsBegin)
                    {
                        Count++;                      
                        Random rd = new Random();
                        int Sleep = RapidInterval[rd.Next(0, 50)];
                        int ms = 0;
                        monitoringLoadingData(out ms);                       
                        Console.WriteLine("[{0}]次，间隔[{1}]秒,耗时[{2}]毫秒",Count, Math.Round(Sleep *1.0/1000,2), ms);
                        if (Sleep> ms)
                        {
                            System.Threading.Thread.Sleep(Sleep-ms);
                        }                       
                    }
                    else
                    {
                        System.Threading.Thread.Sleep(1000);
                    }
                }
            });
        }

        /// <summary>
        /// 开始监视-请求数据
        /// </summary>
        /// <param name="ms"></param>
        /// <returns></returns>
        public int monitoringLoadingData(out int ms)
        {
            System.Diagnostics.Stopwatch stopwatch = new System.Diagnostics.Stopwatch();
            stopwatch.Start(); //  开始监视代码
            GetGoodsList getGoods = new GetGoodsList();
            List<ChoicenessModel> model = getGoods.getGoodsList(ProductTypes.NikeChoiceness);
            List<ChoicenessModel> model1 = getGoods.getGoodsList(ProductTypes.in_stock);
            List<ChoicenessModel> model3 = model.Concat(model1).ToList();
            SetDT(model3);
            stopwatch.Stop(); //  停止监视              
            ms = (int)Math.Round(stopwatch.Elapsed.TotalMilliseconds, 0);  //  毫秒数 
            return model3.Count;
        }

        /// <summary>
        /// 图片和单品页打开
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex==7 || e.ColumnIndex == 8)
            {
                string url = dataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString();
                System.Diagnostics.Process.Start(url);
            }           
        }
    }
}
