﻿namespace NikePanicBuying
{
    partial class Nikes
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.outputBlock = new System.Windows.Forms.ListBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.ProductType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Pid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fullTitle = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.title = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.subtitle = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.descriptionHeading = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.currentPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.productImageUrl = new System.Windows.Forms.DataGridViewLinkColumn();
            this.Slug = new System.Windows.Forms.DataGridViewLinkColumn();
            this.genders = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.skus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colorDescription = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(1175, 370);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(118, 48);
            this.button1.TabIndex = 0;
            this.button1.Text = "开始监控";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // outputBlock
            // 
            this.outputBlock.BackColor = System.Drawing.SystemColors.Window;
            this.outputBlock.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.outputBlock.ForeColor = System.Drawing.SystemColors.WindowText;
            this.outputBlock.FormattingEnabled = true;
            this.outputBlock.ItemHeight = 12;
            this.outputBlock.Location = new System.Drawing.Point(644, 302);
            this.outputBlock.Name = "outputBlock";
            this.outputBlock.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.outputBlock.Size = new System.Drawing.Size(502, 458);
            this.outputBlock.TabIndex = 1;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ProductType,
            this.Pid,
            this.fullTitle,
            this.title,
            this.subtitle,
            this.descriptionHeading,
            this.currentPrice,
            this.productImageUrl,
            this.Slug,
            this.genders,
            this.skus,
            this.colorDescription});
            this.dataGridView1.Location = new System.Drawing.Point(3, 3);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.Size = new System.Drawing.Size(1312, 275);
            this.dataGridView1.TabIndex = 2;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(646, 285);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 12);
            this.label1.TabIndex = 3;
            this.label1.Text = "输出区:";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(1175, 302);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(118, 48);
            this.button2.TabIndex = 4;
            this.button2.Text = "刷新";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // ProductType
            // 
            this.ProductType.DataPropertyName = "ProductType";
            this.ProductType.HeaderText = "商品类型";
            this.ProductType.Name = "ProductType";
            this.ProductType.ReadOnly = true;
            // 
            // Pid
            // 
            this.Pid.DataPropertyName = "Pid";
            this.Pid.HeaderText = "商品Id";
            this.Pid.Name = "Pid";
            this.Pid.ReadOnly = true;
            // 
            // fullTitle
            // 
            this.fullTitle.DataPropertyName = "fullTitle";
            this.fullTitle.HeaderText = "全称";
            this.fullTitle.Name = "fullTitle";
            this.fullTitle.ReadOnly = true;
            // 
            // title
            // 
            this.title.DataPropertyName = "title";
            this.title.HeaderText = "商品名称";
            this.title.Name = "title";
            this.title.ReadOnly = true;
            // 
            // subtitle
            // 
            this.subtitle.DataPropertyName = "subtitle";
            this.subtitle.HeaderText = "副标题";
            this.subtitle.Name = "subtitle";
            this.subtitle.ReadOnly = true;
            // 
            // descriptionHeading
            // 
            this.descriptionHeading.DataPropertyName = "descriptionHeading";
            this.descriptionHeading.HeaderText = "描述";
            this.descriptionHeading.Name = "descriptionHeading";
            this.descriptionHeading.ReadOnly = true;
            // 
            // currentPrice
            // 
            this.currentPrice.DataPropertyName = "currentPrice";
            this.currentPrice.HeaderText = "价格";
            this.currentPrice.Name = "currentPrice";
            this.currentPrice.ReadOnly = true;
            // 
            // productImageUrl
            // 
            this.productImageUrl.DataPropertyName = "productImageUrl";
            this.productImageUrl.HeaderText = "图片地址";
            this.productImageUrl.Name = "productImageUrl";
            this.productImageUrl.ReadOnly = true;
            this.productImageUrl.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // Slug
            // 
            this.Slug.DataPropertyName = "Slug";
            this.Slug.HeaderText = "单品页地址";
            this.Slug.Name = "Slug";
            this.Slug.ReadOnly = true;
            this.Slug.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // genders
            // 
            this.genders.DataPropertyName = "genders";
            this.genders.HeaderText = "鞋子男女";
            this.genders.Name = "genders";
            this.genders.ReadOnly = true;
            // 
            // skus
            // 
            this.skus.DataPropertyName = "skus";
            this.skus.HeaderText = "鞋子尺码";
            this.skus.Name = "skus";
            this.skus.ReadOnly = true;
            // 
            // colorDescription
            // 
            this.colorDescription.DataPropertyName = "colorDescription";
            this.colorDescription.HeaderText = "颜色";
            this.colorDescription.Name = "colorDescription";
            this.colorDescription.ReadOnly = true;
            // 
            // Nikes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1316, 774);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.outputBlock);
            this.Controls.Add(this.button1);
            this.Name = "Nikes";
            this.Load += new System.EventHandler(this.Nike_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ListBox outputBlock;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProductType;
        private System.Windows.Forms.DataGridViewTextBoxColumn Pid;
        private System.Windows.Forms.DataGridViewTextBoxColumn fullTitle;
        private System.Windows.Forms.DataGridViewTextBoxColumn title;
        private System.Windows.Forms.DataGridViewTextBoxColumn subtitle;
        private System.Windows.Forms.DataGridViewTextBoxColumn descriptionHeading;
        private System.Windows.Forms.DataGridViewTextBoxColumn currentPrice;
        private System.Windows.Forms.DataGridViewLinkColumn productImageUrl;
        private System.Windows.Forms.DataGridViewLinkColumn Slug;
        private System.Windows.Forms.DataGridViewTextBoxColumn genders;
        private System.Windows.Forms.DataGridViewTextBoxColumn skus;
        private System.Windows.Forms.DataGridViewTextBoxColumn colorDescription;
    }
}

