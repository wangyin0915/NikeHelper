﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nike.Model
{
    //废弃的
    ///*  1   */
    //#region 1)GoodsModels
    ///// <summary>
    ///// 1)GoodsModels
    ///// </summary>
    //public class GoodsModels
    //{
    //    public pages pages { get; set; }
    //    public List<Objects> Objects { get; set; }
    //}
    //#endregion

    ///*  2   */
    //#region 2)GoodsModels_pages
    ///// <summary>
    ///// 2)GoodsModels_pages
    ///// </summary>
    //public class pages
    //{
    //    /// <summary>
    //    /// 
    //    /// </summary>
    //    public string prev { get; set; }
    //    /// <summary>
    //    /// 下一页地址
    //    /// </summary>
    //    public string next { get; set; }
    //    /// <summary>
    //    /// 总页数
    //    /// </summary>
    //    public int totalPages { get; set; }
    //    /// <summary>
    //    /// 总条数
    //    /// </summary>
    //    public int totalResources { get; set; }
    //}


    //#endregion
    //#region 2)GoodsModels_Objects
    ///// <summary>
    ///// 2)GoodsModels_Objects
    ///// </summary>
    //public class Objects
    //{
    //    public string id { get; set; }
    //    public string lastFetchTime { get; set; }       
    //    public PublishedContent publishedContent { get; set; }
    //    public List<ProductInfo> productInfo { get; set; }
    //}
    //#endregion

    ///*  3   */
    //#region 3)GoodsModels_Objects_PublishedContent
    ///// <summary>
    ///// 3)GoodsModels_Objects_PublishedContent
    ///// </summary>
    //public class PublishedContent
    //{
    //    public List<PublishedContent_Nodes> nodes { get; set; }
    //    public PublishedContent_Properties properties { get; set; }
    //}
    //#endregion
    //#region 3)GoodsModels_Objects_ProductInfo

    ///// <summary>
    ///// 3)GoodsModels_Objects_ProductInfo
    ///// </summary>
    //public class ProductInfo
    //{
    //    public MerchProduct merchProduct { get; set; }
    //    public MerchPrice merchPrice { get; set; }
    //    public Availability availability { get; set; }
    //    public LaunchView launchView { get; set; }
    //    public ProductContent productContent { get; set; }
    //    public ImageUrls imageUrls { get; set; }
    //    public List<Skus> skus { get; set; }
    //    public List<AvailableSkus> availableSkus { get; set; }
    //    public SocialInterest socialInterest { get; set; }
    //}
    //#endregion

    ///*  4   */
    //#region 4)GoodsModels_Objects_PublishedContent_Nodes
    ///// <summary>
    ///// 4)GoodsModels_Objects_PublishedContent_Nodes
    ///// </summary>
    //public class PublishedContent_Nodes
    //{
    //    public List<Nodes> nodes { get; set; }
    //    public string subType { get; set; }
    //    public string id { get; set; }
    //    public string type { get; set; }
    //    public string version { get; set; }
    //    public PublishedContent_Nodes_Properties properties { get; set; }
    //}
    //#endregion
    //#region 4)GoodsModels_Objects_PublishedContent_Properties
    ///// <summary>
    ///// 4)GoodsModels_Objects_PublishedContent_Properties
    ///// </summary>
    //public class PublishedContent_Properties
    //{
    //    public Custom custom { get; set; }
    //    public Publish publish { get; set; }
    //    public string threadType { get; set; }
    //    public List<string> relatedThreads { get; set; }
    //    public Seo seo { get; set; }
    //    public List<Products> products { get; set; }
    //    public CoverCard coverCard { get; set; }
    //}
    //#endregion


    //#region 4)GoodsModels_Objects_ProductInfo_MerchProduct
    ///// <summary>
    ///// 4)GoodsModels_Objects_ProductInfo_MerchProduct
    ///// </summary>
    //public class MerchProduct
    //{
    //    public string id { get; set; }
    //    public string snapshotId { get; set; }
    //    public string modificationDate { get; set; }      
    //    public string status { get; set; }
    //    public string merchGroup { get; set; }
    //    public string styleCode { get; set; }
    //    public string colorCode { get; set; }
    //    public string styleColor { get; set; }
    //    public string pid { get; set; }
    //    public string catalogId { get; set; }
    //    public string productGroupId { get; set; }
    //    public string brand { get; set; }
    //    public List<string> channels { get; set; }
    //    public List<ConsumerChannels> consumerChannels { get; set; }
    //    public List<string> legacyCatalogIds { get; set; }
    //    public List<string> genders { get; set; }
    //    public List<ValueAddedServices> valueAddedServices { get; set; }
    //    public List<string> sportTags { get; set; }
    //    public List<string> classificationConcepts { get; set; }
    //    public List<TaxonomyAttributes> taxonomyAttributes { get; set; }
    //    public List<string> commerceCountryInclusions { get; set; }
    //    public List<string> commerceCountryExclusions { get; set; }
    //    public List<string> abTestValues { get; set; }
    //    public ProductRollup productRollup { get; set; }
    //    public int quantityLimit { get; set; }
    //    public string styleType { get; set; }
    //    public string productType { get; set; }
    //    public string publishType { get; set; }
    //    public bool mainColor { get; set; }
    //    public bool exclusiveAccess { get; set; }
    //    public bool hardLaunch { get; set; }

    //    public bool hidePayment { get; set; }

    //    public bool hideFromCSR { get; set; }

    //    public bool hideFromSearch { get; set; }
    //    public string commercePublishDate { get; set; }
    //    public string commerceStartDate { get; set; }
    //    public string resourceType { get; set; }

    //    public Links links { get; set; }
    //}
    //#endregion
    //#region  4)GoodsModels_Objects_ProductInfo_MerchPrice
    ///// <summary>
    ///// 4)GoodsModels_Objects_ProductInfo_MerchPrice
    ///// </summary>
    //public class MerchPrice
    //{
    //    public string id { get; set; }
    //    public string snapshotId { get; set; }
    //    public string productId { get; set; }
    //    public string parentId { get; set; }
    //    public string parentType { get; set; }
    //    public string modificationDate { get; set; }       
    //    public string country { get; set; }
    //    public int msrp { get; set; }
    //    public int fullPrice { get; set; }
    //    public int currentPrice { get; set; }
    //    public string currency { get; set; }
    //    public bool discounted { get; set; }
    //    public List<string> promoInclusions { get; set; }
    //    public List<string> promoExclusions { get; set; }
    //    public string resourceType { get; set; }
    //    public Links links { get; set; }
    //}
    //#endregion
    //#region 4)GoodsModels_Objects_ProductInfo_Availability
    ///// <summary>
    ///// 4)GoodsModels_Objects_ProductInfo_Availability
    ///// </summary>
    //public class Availability
    //{
    //    public string id { get; set; }
    //    public string productId { get; set; }
    //    public string resourceType { get; set; }
    //    public Links links { get; set; }
    //    public bool available { get; set; }
    //}
    //#endregion
    //#region 4)GoodsModels_Objects_ProductInfo_LaunchView
    ///// <summary>
    ///// 4)GoodsModels_Objects_ProductInfo_LaunchView
    ///// </summary>
    //public class LaunchView
    //{
    //    public string id { get; set; }
    //    public string resourceType { get; set; }
    //    public string productId { get; set; }
    //    public string method { get; set; }
    //    public string startEntryDate { get; set; }       
    //    public string stopEntryDate { get; set; }      
    //    public Links links { get; set; }
    //}
    //#endregion
    //#region 4)GoodsModels_Objects_ProductInfo_ProductContent
    ///// <summary>
    ///// 4)GoodsModels_Objects_ProductInfo_ProductContent
    ///// </summary>
    //public class ProductContent
    //{
    //    public string globalPid { get; set; }
    //    public string langLocale { get; set; }
    //    public string colorDescription { get; set; }
    //    public string slug { get; set; }
    //    public string fullTitle { get; set; }
    //    public string title { get; set; }
    //    public string subtitle { get; set; }
    //    public string descriptionHeading { get; set; }
    //    public string description { get; set; }
    //    public string pdpGeneral { get; set; }
    //    public string techSpec { get; set; }
    //    public string sizeChart { get; set; }
    //    public List<Colors> colors { get; set; }
    //    public List<string> bestFor { get; set; }
    //    public List<string> athletes { get; set; }
    //    public List<Widths> widths { get; set; }
    //}
    //#endregion
    //#region 4)GoodsModels_Objects_ProductInfo_ImageUrls
    ///// <summary>
    ///// 4)GoodsModels_Objects_ProductInfo_ImageUrls
    ///// </summary>
    //public class ImageUrls
    //{
    //    public string productImageUrl { get; set; }
    //}
    //#endregion
    //#region 4)GoodsModels_Objects_ProductInfo_Skus
    ///// <summary>
    ///// 4)GoodsModels_Objects_ProductInfo_Skus
    ///// </summary>
    //public class Skus
    //{
    //    public string id { get; set; }
    //    public string snapshotId { get; set; }
    //    public string productId { get; set; }
    //    public string parentId { get; set; }
    //    public string parentType { get; set; }
    //    public string catalogSkuId { get; set; }
    //    public string modificationDate { get; set; }
    //    public string merchGroup { get; set; }
    //    public string stockKeepingUnitId { get; set; }
    //    public string gtin { get; set; }
    //    public string nikeSize { get; set; }
    //    public List<CountrySpecifications> countrySpecifications { get; set; }
    //    public string resourceType { get; set; }
    //    public Links links { get; set; }
    //}
    //#endregion
    //#region 4)GoodsModels_Objects_ProductInfo_AvailableSkus
    ///// <summary>
    ///// 4)GoodsModels_Objects_ProductInfo_AvailableSkus
    ///// </summary>
    //public class AvailableSkus
    //{
    //    public string id { get; set; }
    //    public string productId { get; set; }
    //    public string resourceType { get; set; }
    //    public Links links { get; set; }
    //    public bool available { get; set; }
    //    public string level { get; set; }
    //    public string skuId { get; set; }
    //}
    //#endregion
    //#region 4)GoodsModels_Objects_ProductInfo_SocialInterest
    ///// <summary>
    ///// 4)GoodsModels_Objects_ProductInfo_SocialInterest
    ///// </summary>
    //public class SocialInterest
    //{
    //    public string id { get; set; }
    //}


    //#endregion

    ///*  5   */
    //#region  5)GoodsModels_Objects_PublishedContent_Nodes_Nodes
    ///// <summary>
    ///// 5)GoodsModels_Objects_PublishedContent_Nodes_Nodes
    ///// </summary>
    //public class Nodes
    //{
    //    /// <summary>
    //    /// image
    //    /// </summary>
    //    public string subType { get; set; }
    //    /// <summary>
    //    /// 076e572f-2ecc-419e-8575-f3bcbb3665c4
    //    /// </summary>
    //    public string id { get; set; }
    //    /// <summary>
    //    /// card
    //    /// </summary>
    //    public string type { get; set; }
    //    /// <summary>
    //    /// 1533334951693
    //    /// </summary>
    //    public string version { get; set; }
    //    /// <summary>
    //    /// Properties
    //    /// </summary>
    //    public PublishedContent_Nodes_Nodes_properties properties { get; set; }
    //}
    //#endregion
    //#region 5)GoodsModels_Objects_PublishedContent_Nodes_Properties
    //public class PublishedContent_Nodes_Properties
    //{
    //    /// <summary>
    //    /// Product
    //    /// </summary>
    //    public List<string> product { get; set; }
    //    /// <summary>
    //    /// carousel
    //    /// </summary>
    //    public string containerType { get; set; }
    //    /// <summary>
    //    /// Loop
    //    /// </summary>
    //    public bool loop { get; set; }
    //    /// <summary>
    //    /// 男子运动鞋
    //    /// </summary>
    //    public string subtitle { get; set; }
    //    /// <summary>
    //    /// dark
    //    /// </summary>
    //    public string colorTheme { get; set; }
    //    /// <summary>
    //    /// AutoPlay
    //    /// </summary>
    //    public bool autoPlay { get; set; }
    //    /// <summary>
    //    /// <p>数十年前，Air Max 95 作为在前足部位搭载可视 Nike Air 缓震配置的新颖鞋款进入大众视野。如今，Nike Air Max 95 OG MC SP " ERDL Party " 为夏日焕新演绎经典鞋款。将鞋面作为画板，并将混搭的迷彩图案和色彩融入其中，展现个性魅力，优质皮革装饰令别出心裁的设计更显别致。</p>
    //    /// <p>一旦您提交订单即表明您同意下述条款</p>
    //    /// </summary>
    //    public string body { get; set; }
    //    /// <summary>
    //    /// Nike Air Max 95 ERDL Party
    //    /// </summary>
    //    public string title { get; set; }
    //    /// <summary>
    //    /// Actions
    //    /// </summary>
    //    public List<Actions> actions { get; set; }
    //    /// <summary>
    //    /// Speed
    //    /// </summary>
    //    public int speed { get; set; }
    //}
    //#endregion

    //#region 5)GoodsModels_Objects_PublishedContent_Properties_Custom   
    ///// <summary>
    ///// 5)GoodsModels_Objects_PublishedContent_Properties_Custom
    ///// </summary>
    //public class Custom
    //{
    //    public List<string> tags { get; set; }
    //}
    //#endregion
    //#region 5)GoodsModels_Objects_PublishedContent_Properties_Publish
    ///// <summary>
    ///// 5)GoodsModels_Objects_PublishedContent_Properties_Publish
    ///// </summary>
    //public class Publish
    //{
    //    public List<string> collections { get; set; }
    //}
    //#endregion
    //#region 5)GoodsModels_Objects_PublishedContent_Properties_Seo
    ///// <summary>
    ///// 5)GoodsModels_Objects_PublishedContent_Properties_Seo
    ///// </summary>
    //public class Seo
    //{
    //    public string keywords { get; set; }
    //    public string description { get; set; }
    //    public bool doNotIndex { get; set; }
    //    public string title { get; set; }
    //    public string slug { get; set; }
    //}
    //#endregion
    //#region 5)GoodsModels_Objects_PublishedContent_Properties_Products
    ///// <summary>
    ///// 5)GoodsModels_Objects_PublishedContent_Properties_Products
    ///// </summary>
    //public class Products
    //{
    //    public string productId { get; set; }
    //    public string styleColor { get; set; }
    //}
    //#endregion
    //#region 5)GoodsModels_Objects_PublishedContent_Properties_CoverCard
    ///// <summary>
    ///// 5)GoodsModels_Objects_PublishedContent_Properties_CoverCard
    ///// </summary>
    //public class CoverCard
    //{
    //    /// <summary>
    //    /// image
    //    /// </summary>
    //    public string subType { get; set; }
    //    /// <summary>
    //    /// 55ef07f3-9aa6-48f6-b086-5da8017dd572
    //    /// </summary>
    //    public string id { get; set; }
    //    /// <summary>
    //    /// card
    //    /// </summary>
    //    public string type { get; set; }
    //    /// <summary>
    //    /// 1533345883932
    //    /// </summary>
    //    public string version { get; set; }
    //    /// <summary>
    //    /// Properties
    //    /// </summary>
    //    public CoverCard_Properties properties { get; set; }
    //}
    //#endregion

    //#region 5)GoodsModels_Objects_ProductInfo_MerchProduct_ConsumerChannels
    ///// <summary>
    ///// 5)GoodsModels_Objects_ProductInfo_MerchProduct_ConsumerChannels
    ///// </summary>
    //public class ConsumerChannels
    //{
    //    public string id { get; set; }
    //    public string resourceType { get; set; }
    //}
    //#endregion
    //#region 5)GoodsModels_Objects_ProductInfo_MerchProduct_ValueAddedServices
    ///// <summary>
    ///// 5)GoodsModels_Objects_ProductInfo_MerchProduct_ValueAddedServices
    ///// </summary>
    //public class ValueAddedServices
    //{
    //    public string id { get; set; }
    //    public string vasType { get; set; }
    //}
    //#endregion
    //#region 5)GoodsModels_Objects_ProductInfo_MerchProduct_TaxonomyAttributes
    ///// <summary>
    ///// 5)GoodsModels_Objects_ProductInfo_MerchProduct_TaxonomyAttributes
    ///// </summary>
    //public class TaxonomyAttributes
    //{
    //    public string resourceType { get; set; }
    //    public List<string> ids { get; set; }
    //}
    //#endregion
    //#region 5)GoodsModels_Objects_ProductInfo_MerchProduct_ProductRollup
    ///// <summary>
    ///// 5)GoodsModels_Objects_ProductInfo_MerchProduct_ProductRollup
    ///// </summary>
    //public class ProductRollup
    //{
    //    public string type { get; set; }
    //    public string key { get; set; }
    //}
    //#endregion
    //#region 5)GoodsModels_Objects_ProductInfo_ProductContent_Colors
    ///// <summary>
    ///// 5)GoodsModels_Objects_ProductInfo_ProductContent_Colors
    ///// </summary>
    //public class Colors
    //{
    //    public string type { get; set; }
    //    public string name { get; set; }
    //    public string hex { get; set; }
    //}
    //#endregion
    //#region 5)GoodsModels_Objects_ProductInfo_ProductContent_Widths
    ///// <summary>
    ///// 5)GoodsModels_Objects_ProductInfo_ProductContent_Widths
    ///// </summary>
    //public class Widths
    //{
    //    public string value { get; set; }
    //    public string localizedValue { get; set; }
    //}
    //#endregion
    //#region 5)GoodsModels_Objects_ProductInfo_Skus_CountrySpecifications
    ///// <summary>
    ///// 5)GoodsModels_Objects_ProductInfo_Skus_CountrySpecifications
    ///// </summary>
    //public class CountrySpecifications
    //{
    //    /// <summary>
    //    /// CN
    //    /// </summary>
    //    public string country { get; set; }
    //    /// <summary>
    //    /// 36
    //    /// </summary>
    //    public string localizedSize { get; set; }
    //    /// <summary>
    //    /// TaxInfo
    //    /// </summary>
    //    public TaxInfo taxInfo { get; set; }
    //}
    //#endregion

    ///*  6   */
    //#region 6)GoodsModels_Objects_PublishedContent_Nodes_Nodes_Properties
    ///// <summary>
    ///// 6)GoodsModels_Objects_PublishedContent_Nodes_Nodes_Properties
    ///// </summary>
    //public class PublishedContent_Nodes_Nodes_properties
    //{
    //    public string portraitId { get; set; }

    //    public string squarishURL { get; set; }

    //    public List<string> product { get; set; }

    //    public string landscapeId { get; set; }

    //    public string altText { get; set; }

    //    public string portraitURL { get; set; }

    //    public string landscapeURL { get; set; }

    //    public Nodes_Nodes_Properties__Portrait_Squarish_Landscape portrait { get; set; }

    //    public Nodes_Nodes_Properties__Portrait_Squarish_Landscape squarish { get; set; }

    //    public string title { get; set; }

    //    public string squarishId { get; set; }

    //    public string subtitle { get; set; }

    //    public string colorTheme { get; set; }

    //    public List<string> actions { get; set; }

    //    public Nodes_Nodes_Properties__Portrait_Squarish_Landscape landscape { get; set; }
    //}
    //#endregion
    //#region 6)GoodsModels_Objects_PublishedContent_Nodes_Properties_Actions
    ///// <summary>
    ///// 6)GoodsModels_Objects_PublishedContent_Nodes_Properties_Actions
    ///// </summary>
    //public class Actions
    //{
    //    /// <summary>
    //    /// cta_buying_tools
    //    /// </summary>
    //    public string actionType { get; set; }
    //    /// <summary>
    //    /// Product
    //    /// </summary>
    //    public Product product { get; set; }
    //    /// <summary>
    //    /// AR4473-001
    //    /// </summary>
    //    public string destinationId { get; set; }
    //}
    //#endregion
    //#region 6)GoodsModels_Objects_PublishedContent_Properties_CoverCard_Properties
    ///// <summary>
    ///// 6)GoodsModels_Objects_PublishedContent_Properties_CoverCard_Properties
    ///// </summary>
    //public class CoverCard_Properties
    //{
    //    public string portraitId { get; set; }
    //    public string squarishURL { get; set; }
    //    public List<string> product { get; set; }
    //    public string landscapeId { get; set; }
    //    public string altText { get; set; }
    //    public string portraitURL { get; set; }
    //    public string landscapeURL { get; set; }
    //    public CoverCard_Properties__Portrait_Squarish_Landscape portrait { get; set; }
    //    public CoverCard_Properties__Portrait_Squarish_Landscape squarish { get; set; }
    //    public string title { get; set; }
    //    public string squarishId { get; set; }
    //    public string subtitle { get; set; }
    //    public string colorTheme { get; set; }
    //    public List<string> actions { get; set; }
    //    public CoverCard_Properties__Portrait_Squarish_Landscape landscape { get; set; }
    //}
    //#endregion
    //#region 6)GoodsModels_Objects_ProductInfo_Skus_CountrySpecifications_TaxInfo
    ///// <summary>
    ///// 6)GoodsModels_Objects_ProductInfo_Skus_CountrySpecifications_TaxInfo
    ///// </summary>
    //public class TaxInfo
    //{
    //    /// <summary>
    //    /// Vat
    //    /// </summary>
    //    public int vat { get; set; }
    //}
    //#endregion

    ///*  7   */
    //#region 7)GoodsModels_Objects_PublishedContent_Nodes_Nodes_Properties__Portrait_Squarish_Landscape
    ///// <summary>
    ///// 7)GoodsModels_Objects_PublishedContent_Nodes_Nodes_Properties__Portrait_Squarish_Landscape
    ///// </summary>
    //public class Nodes_Nodes_Properties__Portrait_Squarish_Landscape
    //{
    //    public int aspectRatio { get; set; }  
    //    public string id { get; set; }
    //    public string type { get; set; }
    //    public string url { get; set; }
    //}
    //#endregion
    //#region 7)GoodsModels_Objects_PublishedContent_Nodes_Properties_Actions_Product
    ///// <summary>
    ///// 7)GoodsModels_Objects_PublishedContent_Nodes_Properties_Actions_Product
    ///// </summary>
    //public class Product
    //{
    //    /// <summary>
    //    /// 23998b7b-25aa-5e6b-a41b-252bf665d5db
    //    /// </summary>
    //    public string productId { get; set; }
    //    /// <summary>
    //    /// AR4473-001
    //    /// </summary>
    //    public string styleColor { get; set; }
    //}
    //#endregion
    //#region 7)GoodsModels_Objects_PublishedContent_Properties_CoverCard_Properties__Portrait_Squarish_Landscape
    ///// <summary>
    ///// 7)GoodsModels_Objects_PublishedContent_Properties_CoverCard_Properties__Portrait_Squarish_Landscape
    ///// </summary>
    //public class CoverCard_Properties__Portrait_Squarish_Landscape
    //{
    //    public string view { get; set; }
    //    public string id { get; set; }
    //    public string type { get; set; }
    //    public string url { get; set; }
    //}
    //#endregion





    ///// <summary>
    ///// 5)GoodsModels_Objects_ProductInfo_MerchProduct_Links
    ///// 5)GoodsModels_Objects_ProductInfo_MerchPrice_Links
    ///// 5)GoodsModels_Objects_ProductInfo_Availability_Links
    ///// 5)GoodsModels_Objects_ProductInfo_LaunchView_Links
    ///// 5)GoodsModels_Objects_ProductInfo_Skus_Links
    ///// 5)GoodsModels_Objects_ProductInfo_AvailableSkus_Links
    ///// </summary>
    //public class Links
    //{
    //    public Self self { get; set; }
    //}
    ///// <summary>
    ///// 6)GoodsModels_Objects_ProductInfo_MerchProduct_Links_Self
    ///// 6)GoodsModels_Objects_ProductInfo_MerchPrice_Links_Self
    ///// 6)GoodsModels_Objects_ProductInfo_Availability_Links_Self
    ///// 6)GoodsModels_Objects_ProductInfo_LaunchView_Links_Self
    ///// 6)GoodsModels_Objects_ProductInfo_Skus_Links_Self
    ///// 6)GoodsModels_Objects_ProductInfo_AvailableSkus_Links_Self
    ///// </summary>
    //public class Self
    //{
    //    public string refstr { get; set; }
    //}

}


