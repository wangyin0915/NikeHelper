﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Nike.EnumHelper.ProductTypesEnum;

namespace Nike.Model
{
    public class ChoicenessModel
    {
        private ProductTypes productTypes;
        /// <summary>
        /// 商品类型
        /// </summary>
        public string ProductType { get {return GetEnumDescription(productTypes); } set { productTypes = (ProductTypes)Convert.ToInt32(value); } }
        /// <summary>
        /// 商品Id
        /// </summary>
        public string Pid { get; set; }
        /// <summary>
        /// 副标题
        /// </summary>
        public string subtitle { get; set; }
        /// <summary>
        /// 商品名称
        /// </summary>
        public string title { get; set; }
        /// <summary>
        /// 全称
        /// </summary>
        public string fullTitle { get; set; }
        /// <summary>
        /// 描述
        /// </summary>
        public string descriptionHeading { get; set; }
        public string currentPrice { get; set; }
        private string ProductImageUrl { get; set; }
        /// <summary>
        /// 图片地址
        /// </summary>
        public string productImageUrl { get { return string.Format("{0}_A_PREM?$SNKRS_COVER_WD$&align=0,1", ProductImageUrl); } set { ProductImageUrl = value; } }
        private string slug;
        /// <summary>
        /// 单品页地址
        /// </summary>
        public string Slug { get { return string.Format("https://www.nike.com/cn/launch/t/{0}/" ,slug); } set { slug = value; } }
        ///// <summary>
        ///// 产品组ID
        ///// </summary>
        //public string productGroupId { get; set; }
       
        /// <summary>
        /// 鞋子男女(MEN,WOMEN)
        /// </summary>
        public string genders { get; set; }
        /// <summary>
        /// 鞋子尺码()
        /// </summary>
        public string skus { get; set; }
        /// <summary>
        /// 颜色
        /// </summary>
        public string colorDescription { get; set; }
    }

}
