﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Nike.EnumHelper
{
    public class ProductTypesEnum
    {
        /// <summary>
        /// 商品类型
        /// </summary>
        public enum ProductTypes
        {
            [Description("精选")]
            NikeChoiceness = 1,
            [Description("热卖中")]
            in_stock = 2,
            [Description("新品预览")]
            upcoming = 3
        }

        /// <summary>
        /// 枚举字段描述列表
        /// </summary>
        /// <param name="t"></param>
        /// <returns></returns>

        public static IList<string> GetEnumDescriptionList(Type t)
        {
            var valueDescList = Enum.GetValues(t).Cast<Enum>().Select(m => GetEnumDescription(m)).ToList();
            return valueDescList;
        }
        /// <summary>
        /// 获取枚举的描述信息（Description特性）
        /// </summary>
        /// <param name="enumValue">枚举值</param>
        /// <returns></returns>

        public static string GetEnumDescription(Enum enumValue)
        {
            Type type = enumValue.GetType();
            FieldInfo fi = type.GetField(enumValue.ToString());
            object[] attrs = fi.GetCustomAttributes(typeof(DescriptionAttribute), true);
            if (attrs.Length > 0)
                return ((DescriptionAttribute)attrs[0]).Description;
            return "";
        }

    }
}
