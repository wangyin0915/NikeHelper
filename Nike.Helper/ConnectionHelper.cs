﻿using CsharpHttpHelper;
using CsharpHttpHelper.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using static Nike.EnumHelper.ProductTypesEnum;

namespace Nike.Helper
{
    public class ConnectionHelper
    {
        const string Nike = "https://www.nike.com/cn/zh_cn/";
        const string baidu = "https://www.baidu.com/";
        const string retNike = "https://m.nike.com/";
        const string NiekApi = "https://api.nike.com";
        /// <summary>
        /// 精选
        /// </summary>
        const string NikeChoiceness = "/product_feed/threads/v2/?anchor=0&count={0}&filter=marketplace%28CN%29&filter=language%28zh-Hans%29&filter=channelId%28010794e5-35fe-4e32-aaff-cd2c74f89d61%29&filter=exclusiveAccess%28true%2Cfalse%29&fields=active&fields=id&fields=lastFetchTime&fields=productInfo&fields=publishedContent.nodes&fields=publishedContent.properties.coverCard&fields=publishedContent.properties.productCard&fields=publishedContent.properties.products&fields=publishedContent.properties.publish.collections&fields=publishedContent.properties.relatedThreads&fields=publishedContent.properties.seo&fields=publishedContent.properties.threadType&fields=publishedContent.properties.custom";
        /// <summary>
        /// 热卖中
        /// </summary>
        const string in_stock = "/product_feed/threads/v2/?anchor=0&count={0}&filter=marketplace%28CN%29&filter=language%28zh-Hans%29&filter=inStock%28true%29&filter=productInfo.merchPrice.discounted%28false%29&filter=channelId%28010794e5-35fe-4e32-aaff-cd2c74f89d61%29&filter=exclusiveAccess%28true%2Cfalse%29&fields=active&fields=id&fields=lastFetchTime&fields=productInfo&fields=publishedContent.nodes&fields=publishedContent.properties.coverCard&fields=publishedContent.properties.productCard&fields=publishedContent.properties.products&fields=publishedContent.properties.publish.collections&fields=publishedContent.properties.relatedThreads&fields=publishedContent.properties.seo&fields=publishedContent.properties.threadType&fields=publishedContent.properties.custom";
        /// <summary>
        /// 新品预览
        /// </summary>
        const string upcoming = "";

        /// <summary>
        /// 测试连接
        /// </summary>
        /// <param name="IsNike"></param>
        /// <param name="milliseconds"></param>
        /// <returns></returns>
        public static bool IsHaveInternet(bool IsNike, out double milliseconds)
        {
            System.Diagnostics.Stopwatch stopwatch = new System.Diagnostics.Stopwatch();
            stopwatch.Start(); //  开始监视代码
            bool ret = false;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;//当出现“请求被中止: 未能创建 SSL/TLS 安全通道”时需要配置此属性
            HttpHelper http = new HttpHelper();
            HttpItem item = new HttpItem()
            {
                URL = IsNike == true ? Nike : baidu,//URL     必需项 
                Method = "Get",//URL     可选项 默认为Get  
                Timeout = 60000,//连接超时时间     可选项默认为100000  
                ReadWriteTimeout = 30000,//写入Post数据超时时间     可选项默认为30000  
                Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",//    可选项有默认值  
                UserAgent = "Mozilla/5.0 (iPhone; CPU iPhone OS 11_0 like Mac OS X) AppleWebKit/604.1.38 (KHTML, like Gecko) Version/11.0 Mobile/15A372 Safari/604.1",//用户的浏览器类型，版本，操作系统     可选项有默认值  
                Host = "m.nike.com",
                Referer = "https://www.baidu.com/",



                IsToLower = false,//得到的HTML代码是否转成小写     可选项默认转小写  
                Cookie = string.Format("_gscu_207448657=33375526p7mnrp44; geoloc=cc=CN,rc=JX,tp=vhigh,tz=GMT+8,la=28.68,lo=115.88; _abck=8CC6CB3DC5B635DF62F331C5720BE1F3D2C077156030000070E4655B899BA745~-1~eBdhz0qM9hEUMKzU7aoT4j+Oj/uZGQKQGKShPDvsX0Q=~-1~-1; bm_sz=27872589B56C5BD61267E372511E4C81~QAAQFXfA0mqcltVkAQAAVlkEBmb2LAdMnMFFR7y4rH0tNDhVJoj+HzXLM1DJOHG/yJZp/BOSyGa8Olvk0O0TE+UWwRMRV2CM9/IL1bolsYH5/wE6txwuSH5J8+Wba1xQvp4Fyt7EzUS4xSPC3N6FD2SWCx25lzoifGoSZaCE7cKGJzuzX2gI3y7R2mcX; RT=\"dm=nike.com&si=d411b9bc-6417-4d21-b86b-627751eec535&ss=1533400560077&sl=11&tt=23504&obo=0&sh=1533404261205%3D11%3A0%3A23504%2C1533403818891%3D10%3A0%3A21681%2C1533403579574%3D9%3A0%3A18488%2C1533403538997%3D8%3A0%3A16234%2C1533403527407%3D7%3A0%3A14643&bcn=%2F%2F36fb78d5.akstat.io%2F&r=https%3A%2F%2Fm.nike.com%2Fcn%2Fzh_cn%2F&ul={0}\"", GetTimestamp(DateTime.Now)),//字符串Cookie     可选项 
                
                
                ContentType = "text/html",//返回类型    可选项有默认值  
              
                
                Allowautoredirect = true,//是否根据３０１跳转     可选项  
                AutoRedirectCookie = true,//是否自动处理Cookie     可选项  
                                           //CerPath = "d:\123.cer",//证书绝对路径     可选项不需要证书时可以不写这个参数  
                                           //Connectionlimit = 1024,//最大连接数     可选项 默认为1024  
                Postdata = "",//Post数据     可选项GET时不需要写  
                              //ProxyIp = "192.168.1.105：2020",//代理服务器ID     可选项 不需要代理 时可以不设置这三个参数  
                              //ProxyPwd = "123456",//代理服务器密码     可选项  
                              //ProxyUserName = "administrator",//代理服务器账户名     可选项  
                ResultType = ResultType.String,//返回数据类型，是Byte还是String  
                ProtocolVersion = System.Net.HttpVersion.Version11,//获取或设置用于请求的 HTTP 版本。默认为 System.Net.HttpVersion.Version11  
            };
            item.Header.Add("Accept-Encoding", "gzip, deflate, br");
            item.Header.Add("Accept-Language", "zh-CN,zh;q=0.9");
            item.Header.Add("Cache-Control", "max-age=0");          
            item.Header.Add("Upgrade-Insecure-Requests", "1");          
            try
            {
                HttpResult result = http.GetHtml(item);
                if (result.StatusCode == HttpStatusCode.OK)
                {
                    ret = true;
                }
            }
            catch (Exception ex)
            {

            }

            stopwatch.Stop(); //  停止监视              
            milliseconds = Math.Round(stopwatch.Elapsed.TotalMilliseconds, 0);  //  毫秒数 
            return ret;
        }

        /// <summary>
        /// 发起请求，获取数据
        /// </summary>
        /// <param name="productTypes"></param>
        /// <param name="page"></param>
        /// <param name="milliseconds"></param>
        /// <returns></returns>
        public static string GetDataFromAPI(ProductTypes productTypes,int page,out double milliseconds)
        {
            System.Diagnostics.Stopwatch stopwatch = new System.Diagnostics.Stopwatch();
            stopwatch.Start(); //  开始监视代码
            string ret = string.Empty;
            string url = string.Empty;
            switch ((int)productTypes)
            {
                case 1:
                    url = string.Format(NiekApi + NikeChoiceness, page);
                    break;
                case 2:
                    url= string.Format(NiekApi + in_stock, page);
                    break;
                case 3:                  
                    break;
                default:
                    break;

            }
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;//当出现“请求被中止: 未能创建 SSL/TLS 安全通道”时需要配置此属性
            HttpHelper http = new HttpHelper();
            if (string.IsNullOrEmpty(url))
            {
                stopwatch.Stop(); //  停止监视              
                milliseconds = Math.Round(stopwatch.Elapsed.TotalMilliseconds, 0);  //  毫秒数 
                return url;
            }
            HttpItem item = new HttpItem()
            {
                URL= url,
                Method = "Get",
                Timeout = 60000,
                ReadWriteTimeout = 30000,
                Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",//    可选项有默认值  
                UserAgent = "Mozilla/5.0 (iPhone; CPU iPhone OS 11_0 like Mac OS X) AppleWebKit/604.1.38 (KHTML, like Gecko) Version/11.0 Mobile/15A372 Safari/604.1",//用户的浏览器类型，版本，操作系统     可选项有默认值  
                Host = "api.nike.com",
                Referer = "https://www.nike.com/cn/launch/",
                AutoRedirectCookie = true,

                ContentType = "application/json; charset=UTF-8",
               
                Postdata = "",
                ResultType = ResultType.String,
                ProtocolVersion = System.Net.HttpVersion.Version11              
            };
            item.Header.Add("Accept-Encoding", "gzip, deflate, br");
            item.Header.Add("Accept-Language", "zh-CN,zh;q=0.9");
            item.Header.Add("Cache-Control", "private, no-transform, max-age=30");
            item.Header.Add("Upgrade-Insecure-Requests", "1");
            try
            {
                HttpResult result = http.GetHtml(item);
                if (result.StatusCode == HttpStatusCode.OK)
                {
                    ret = result.Html;
                }
            }
            catch (Exception ex)
            {

            }

            stopwatch.Stop(); //  停止监视              
            milliseconds = Math.Round(stopwatch.Elapsed.TotalMilliseconds, 0);  //  毫秒数 
            return ret;
        }


        /// <summary>
        /// 获取时间戳
        /// </summary>
        /// <param name="d"></param>
        /// <returns></returns>
        public static double GetTimestamp(DateTime d)
        {
            TimeSpan ts = d.ToUniversalTime() - new DateTime(1970, 1, 1);
            return Math.Round(ts.TotalMilliseconds, 0);     //精确到毫秒
        }
    }
}
