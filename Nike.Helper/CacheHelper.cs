﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Runtime.Caching;
using System.Web.Caching;

namespace Nike.Helper
{
    public class CacheHelper
    {
        /// <summary>
        /// 设置Cache 如果有该key的Cache,则会remove后再生成
        /// </summary>
        /// <param name="key">缓存关键字</param>
        /// <param name="value">缓存的内容</param>
        /// <param name="seconds">缓存时间:单位(秒) 0 表示永不过期</param>
        /// <returns></returns>
        public static bool SetCache(string key, object value, int seconds)
        {
            if (value == null)
                return false;
            try
            {
                if (HttpRuntime.Cache[key] != null)
                    HttpRuntime.Cache.Remove(key);
                if (seconds <= 0)
                    HttpRuntime.Cache.Insert(key, value);
                else
                    HttpRuntime.Cache.Insert(key, value, null, DateTime.Now.AddSeconds(seconds), Cache.NoSlidingExpiration, System.Web.Caching.CacheItemPriority.NotRemovable, null);
                return true;
            }
            catch { }
            return false;
        }
        /// <summary>
        /// 得到缓存内容
        /// </summary>
        /// <param name="key">缓存关键字</param>
        public static object GetCache(string key)
        {
            try
            {
                if (HttpRuntime.Cache[key] != null)
                    return HttpRuntime.Cache[key];
            }
            catch { }
            return null;
        }

        public static string GetCache(string key, bool toString)
        {
            try
            {
                if (HttpRuntime.Cache[key] != null)
                    return ToString(HttpRuntime.Cache[key]);
            }
            catch { }
            return "";
        }

        public static string GetCacheToString(string key)
        {
            try
            {
                if (HttpRuntime.Cache[key] != null)
                    return ToString(HttpRuntime.Cache[key]);
            }
            catch { }
            return "";
        }

        /// <summary>
        /// 转为字符串
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string ToString(object obj)
        {
            if (obj == null)
                return "";
            return obj.ToString();
        }

        /// <summary>
        /// 是否包含key
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static bool Exists(string key)
        {
            return Contains(key);
        }

        /// <summary>
        /// 是否包含key
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static bool Contains(string key)
        {
            if (HttpRuntime.Cache[key] != null)
                return true;
            return false;
        }

        /// <summary>
        /// 移除key值项
        /// </summary>
        /// <param name="key"></param>
        public static void Remove(string key)
        {
            try
            {
                if (HttpRuntime.Cache[key] != null)
                    HttpRuntime.Cache.Remove(key);
            }
            catch { }
        }

        /// <summary>
        /// 清空所有的Cache
        /// </summary>
        public static void Clare()
        {
            System.Collections.IDictionaryEnumerator caches = HttpRuntime.Cache.GetEnumerator();
            while (caches.MoveNext())
            {
                HttpRuntime.Cache.Remove(caches.Key.ToString());
            }
        }

        /// <summary>
        /// 是否延迟请求
        /// </summary>
        /// <param name="cacheKey">缓存Key</param>
        /// <param name="delaySeconds">延迟秒数</param>
        /// <returns></returns>
        public static bool IsDelay(string cacheKey, int delaySeconds)
        {
            if (CacheHelper.Exists(cacheKey))
                return true;

            CacheHelper.SetCache(cacheKey, "", delaySeconds);
            return false;
        }
    }
}
