﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nike.Helper
{
    /// <summary>
    /// 重写Console.WriteLine和Console.Write  输出到ListBox上
    /// </summary>
    public class TextBoxWriter : System.IO.TextWriter
    {
        int ClearCount = 1000;//设置超出自动清空ListBox
        ListBox lstBox;
        delegate void VoidAction();
        public TextBoxWriter(ListBox box)
        {
            lstBox = box;
        }
        public override void Write(string value)
        {
            VoidAction action = delegate
            {
                lstBox.Items.Insert(0, string.Format("[{0:HH:mm:ss}]{1}", DateTime.Now, value));
            };
            lstBox.BeginInvoke(action);
           
            if (lstBox.Items.Count >= ClearCount)
            {               
                VoidAction actionClear = delegate
                {
                    lstBox.Items.Clear();
                    lstBox.Items.Insert(0, string.Format("{0:yyyy-MM-dd HH:mm:ss}  {1}", DateTime.Now, "清空输出区"));

                };                
                lstBox.BeginInvoke(actionClear);
            }
        }
        public override void WriteLine(string value)
        {
            VoidAction action = delegate
            {
                lstBox.Items.Insert(0, string.Format("{0:yyyy-MM-dd HH:mm:ss}  {1}", DateTime.Now, value));
              
            };
            lstBox.BeginInvoke(action);

            if (lstBox.Items.Count >= ClearCount)
            {                
                VoidAction actionClear = delegate
                {
                    lstBox.Items.Clear();
                    lstBox.Items.Insert(0, string.Format("{0:yyyy-MM-dd HH:mm:ss}  {1}", DateTime.Now, "清空输出区"));
                };                
                lstBox.BeginInvoke(actionClear);
            }
        }
        public override System.Text.Encoding Encoding
        {
            get { return System.Text.Encoding.UTF8; }
        }
    }
}
