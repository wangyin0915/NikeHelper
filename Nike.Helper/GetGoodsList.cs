﻿using Newtonsoft.Json;
using Nike.Model;
using Nike.Model.GoodsModels1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static Nike.EnumHelper.ProductTypesEnum;

namespace Nike.Helper
{
    public class GetGoodsList
    {
        /// <summary>
        /// 请求数据（发起请求，获取精品和热卖的数据）
        /// </summary>
        /// <param name="productTypes"></param>
        /// <param name="Istiming"></param>
        /// <param name="page"></param>
        /// <returns></returns>
        public List<ChoicenessModel> getGoodsList(ProductTypes productTypes, bool Istiming=false, int page=10)
        {
            double milliseconds = 0;
            string RequestStr = ConnectionHelper.GetDataFromAPI(productTypes, page,out milliseconds);

            GoodsModels1 list = new GoodsModels1();
            if (RequestStr.Length>10000)
            {
                try
                {
                    list = (GoodsModels1)JsonConvert.DeserializeObject(RequestStr, typeof(GoodsModels1));
                }
                catch (Exception ex)
                {
                    MessageBox.Show("程序出现错误！请截图给管理员处理！");
                }                
            }
            List<ChoicenessModel> Choicenesslist = DataToChoicenessModel(productTypes, list);
            if (Istiming)
            {
                Console.WriteLine("查询到了{0}条数据，耗时:{1}毫秒", page, milliseconds);
            }
            return Choicenesslist;
        }

        /// <summary>
        ///  转换成简短的实体(这保留我们需要的数据)
        /// </summary>
        /// <param name="productTypes"></param>
        /// <param name="Model"></param>
        /// <returns></returns>
        public List<ChoicenessModel> DataToChoicenessModel(ProductTypes productTypes, GoodsModels1 Model)
        {
            #region MyRegion
            //list.objects.publishedContent.properties.coverCard.properties.subtitle  副标题    女款 AIR FORCE 1 HIGH
            //list.objects.publishedContent.properties.coverCard.properties.title  title    JDI COLLECTION
            //list.objects.productInfo[0].imageUrls.productImageUrl   图片 https://secure-images.nike.com/is/image/DotCom/AO5138_100
            //                                                             https://secure-images.nike.com/is/image/DotCom/AO1021_100_A_PREM?$SNKRS_COVER_WD$&align=0,1
            //                                                                                                                      _A_PREM?$SNKRS_COVER_WD$&align=0,1
            //list.objects.publishedContent.properties.seo.slug 单品页连接  womens-air-force-1-upstep-hi-lx-just-do-it
            //                                                             https://www.nike.com/cn/launch/t/womens-air-force-1-upstep-hi-lx-just-do-it/
            //list.objects.productInfo[0].merchPrice
            //                                      .msrp 零售价
            //                                      .fullPrice 全价
            //                                      .currentPrice 现价 
            #endregion
            List<ChoicenessModel> Choicenesslist = new List<ChoicenessModel>();
            foreach (Objects item in Model.Objects)
            {
                ChoicenessModel Choiceness = new ChoicenessModel();
                Choiceness.ProductType = ((int)productTypes).ToString();
                Choiceness.subtitle = item.productInfo[0].productContent.subtitle;
                Choiceness.title = item.productInfo[0].productContent.title;
                Choiceness.fullTitle = item.productInfo[0].productContent.fullTitle;

                Choiceness.descriptionHeading = item.productInfo[0].productContent.descriptionHeading;
                Choiceness.currentPrice = item.productInfo[0].merchPrice.currentPrice;
                Choiceness.productImageUrl = item.productInfo[0].imageUrls.productImageUrl;

                Choiceness.Slug = item.publishedContent.properties.seo.slug;
                Choiceness.Pid = item.productInfo[0].merchProduct.pid;
                //Choiceness.productGroupId = item.productInfo[0].merchProduct.productGroupId;
                Choiceness.genders = string.Join(",", item.productInfo[0].merchProduct.genders.ToArray()).ToLower();
                Choiceness.colorDescription = item.productInfo[0].productContent.colorDescription;
                string skusStr = string.Empty;
                foreach (Skus skus in item.productInfo[0].skus)
                {
                    skusStr += string.Format("[{0}],",skus.countrySpecifications[0].localizedSize);
                }
                Choiceness.skus = skusStr.Trim(',');
                Choicenesslist.Add(Choiceness);
            }
            return Choicenesslist;
        }
    }
}
